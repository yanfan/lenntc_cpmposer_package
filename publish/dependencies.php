<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */


$dependencies = [];

$appEnv = env('APP_ENV', 'production');
if ($appEnv !== 'dev') {
    $dependencies[\Hyperf\Contract\StdoutLoggerInterface::class] = \App\Log\StdoutLoggerFactory::class;
}
return $dependencies + [
];
