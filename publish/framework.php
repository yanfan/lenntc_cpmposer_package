<?php


declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

return [
    'third' => [
        'shoutu' => [
            ## 正式环境
            'appkey' => '16084347275',
            'appsecret' => 'v3h7DkUkCDM8iCvEQ8qwZ2Kd',
            'url' => 'https://movieapi2.pintoto.cn',
            'spareurl' => 'https://yp-api.taototo.cn',
            ## 测试环境
            'test_appkey' => '10000000000',
            'test_appsecret' => '25f9e794323b453885f5181f1b624d0b',
            'testurl' => 'http://movieapi2-test.taototo.cn'
        ],
        'nianchu' => []
    ]
];
