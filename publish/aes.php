<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

return [
    'key' => 'oxL2qw6FKPwaJRLmCP93tc8UqsLBiOt4',
    'iv' => 'h789tuxpHFcAVK1U',
    'method' => 'AES-128-CBC',
    'options' => 0
];