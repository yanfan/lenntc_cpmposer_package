<?php

declare(strict_types=1);

namespace Lenntc\Framework\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * 错误码枚举类
 * @Constants()
 * Class ErrorCode
 * @package Lenntc\Framework\Constants
 * @method static string getMessage(int $code) 获取错误码信息
 * @method static int getHttpCode(int $code) 获取错误码的httpCode
 */
class ErrorCode extends AbstractConstants
{

    /**
     * @Message("服务器异常(third-party-api)")
     * @HttpCode("500")
     */
    const SERVER_ERROR = 500;

    /**
     * @Message ("非法的参数")
     * @HttpCode ("1001")
     */
    const INVALID_PARAM = 1001;

}