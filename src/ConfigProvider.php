<?php


namespace Lenntc\Framework;


use Hyperf\ExceptionHandler\Listener\ErrorExceptionHandler;
use Lenntc\Framework\Exception\Handler\ValidationExceptionHandler;
use Lenntc\Framework\Middleware\CorsMiddleware;

class ConfigProvider
{
    public function __invoke(): array
    {
        $servieMap = $this->serviceMap();

        return [
            'dependencies' => array_merge($servieMap,[]),
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
            'commands' => [],
            'exceptions' => [
                'handler' => [
                    'http' => [
                        ValidationExceptionHandler::class,
                    ]
                ]
            ],
            'middlewares' => [
                'http' => [
                    CorsMiddleware::class
                ]
            ],
            'listeners' => [
                ErrorExceptionHandler::class
            ],
            'publish' => [
                [
                    'id' => 'framework',
                    'description' => 'framework配置',
                    'source' => __DIR__ . '/../publish/framework.php',
                    'destination' => BASE_PATH . '/config/autoload/framework.php',
                ],
                [
                    'id' => 'dependencies',
                    'description' => '依赖配置',
                    'source' => __DIR__ . '/../publish/dependencies.php',
                    'destination' => BASE_PATH . '/config/autoload/dependencies.php',
                ],
                [
                    'id' => 'aes',
                    'description' => 'AES配置',
                    'source' => __DIR__ . '/../publish/aes.php',
                    'destination' => BASE_PATH . '/config/autoload/aes.php',
                ],
            ],
        ];
    }

    protected function serviceMap(string $path = 'app'): array
    {
        $services = readFileName( BASE_PATH .'/'.$path.'/Service');
        $spacePerfix = ucfirst($path);

        $dependencies = [];
        foreach ($services as $service) {
            $dependencies[$spacePerfix.'\\Contract\\'.$service.'Interface'] = $spacePerfix .'\\Service\\'.$service;
        }
        return $dependencies;
    }
}