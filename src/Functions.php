<?php

declare(strict_types=1);
/**
 * This file is part of Lenntc.
 */

if (! function_exists('readFileName')) {

    /**
     * 读取目录下所有的php文件名称
     * @param string $path
     * @return array
     */
    function readFileName(string $path): array
    {
        $data = [];
        if(!is_dir($path)) {
            return $data;
        }
        $files = scandir($path);
        foreach ($files as $file) {
            if(in_array($file, ['.', '..', '.DS_Store'])) {
                continue;
            }
            $data[] = preg_replace('/(\w+)\.php/', '$1', $file);
        }
        return $data;
    }
}
